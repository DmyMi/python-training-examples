{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Apache Spark\n",
    "\n",
    "<img src=\"images/apache_spark_logo.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"30%\">\n",
    "\n",
    "[Apache Spark](https://spark.apache.org) was first released in 2014. It was originally developed by Matei Zaharia as a class project, and later a PhD dissertation, at University of California, Berkeley. Spark is written in Scala (a JVM language).\n",
    "\n",
    "Apache Spark is a fast and general-purpose cluster computing system. It provides high-level APIs in Java, Scala, Python and R, and an optimized engine that supports general execution graphs.\n",
    "\n",
    "Spark can manage \"big data\" collections with a small set of high-level primitives like `map`, `filter`, `groupby`, and `join`.  With these common patterns we can often handle computations that are more complex than map, but are still structured.\n",
    "\n",
    "It also supports a rich set of higher-level tools including Spark SQL for SQL and structured data processing, MLlib for machine learning, GraphX for graph processing, and Spark Streaming.\n",
    "\n",
    "<img src=\"images/components.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"50%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview\n",
    "Iterative jobs with MapReduce involve a lot of disk I/O for each iteration and stage and dDisk I/O is very slow (even if it is local I/O).\n",
    "\n",
    "<img src=\"images/mr.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"50%\">\n",
    "\n",
    "Basic idea of Spark: as the cost of main memory decreased and large main memories are available in each server -- keep more __data in main memory__.\n",
    "\n",
    "<img src=\"images/spark_vs_mr.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"50%\">\n",
    "\n",
    "It also speeds up operations on the same data.\n",
    "\n",
    "<img src=\"images/spark_vs_mr2.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"50%\">\n",
    "\n",
    "## Spark Framework\n",
    "\n",
    "* Provides a programming abstraction and transparent mechanisms to execute code in parallel on RDDs\n",
    "* Hides complexities of fault-tolerance and slow machines\n",
    "* Manages scheduling and synchronization of the jobs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Resilient Distributed Datasets (RDDs)\n",
    "\n",
    "Data are represented as RDDs\n",
    "* Partitioned/Distributed collections of objects spread across the nodes of a cluster\n",
    "* Stored in main memory (when it is possible) or on local disk\n",
    "\n",
    "Spark programs are written in terms of operations on resilient distributed data sets.\n",
    "\n",
    "RDDs are immutable once constructed.\n",
    "\n",
    "RDDs are built and manipulated through a set of parallel\n",
    "* Transformations\n",
    "    * `map`, `filter`, `join`, …\n",
    "* Actions\n",
    "    * `count`, `collect`, `save`, …\n",
    "\n",
    "RDDs are automatically rebuilt on machine failure. Spark tracks lineage information to efficiently recompute lost data, i.e., for each RDD, Spark knows how it has been constructed and can rebuilt it if a failure occurs. This information is represented by means of a DAG (Direct Acyclic Graph) connecting input data and RDDs.\n",
    "\n",
    "<img src=\"images/rdd.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"50%\">\n",
    "\n",
    "> Note: Spark 1.3 introduced a higher level API for data manipulation called DataFrames, that still use RDDs under the hood. So we will focus on the basic API here.\n",
    "\n",
    "## Partitions\n",
    "A partition is a fundamental unit of data that represents a logical division of a large dataset. Spark operates on data in parallel across a cluster, and partitions are the means by which Spark divides the dataset into smaller, manageable pieces that can be processed independently and in parallel across different nodes of the cluster.\n",
    "\n",
    "Partitions are key to achieving parallelism in Spark. By dividing the dataset into multiple partitions, Spark can distribute the data across the cluster and perform operations on different partitions simultaneously, leveraging multiple processors or nodes. Efficient partitioning can significantly impact the performance of a Spark application. Proper partitioning ensures that data is distributed evenly across the cluster, avoiding scenarios where some nodes are doing much more work than others (data skew).\n",
    "\n",
    "### Characteristics\n",
    "\n",
    "- __Location__: Partitions can reside in memory or on disk, and they can be stored across multiple nodes in a distributed system.\n",
    "- __Size__: The size of a partition is configurable and can affect performance. Smaller partitions may lead to more overhead in terms of scheduling and task management, while larger partitions may not utilize the cluster resources as effectively.\n",
    "- __Number__: The number of partitions can be specified by the user or determined by Spark based on the dataset and the cluster configuration. For example, when reading data from HDFS, Spark might create one partition for each block of the file.\n",
    "\n",
    "### Customization\n",
    "\n",
    "- __Repartitioning__: Spark allows users to repartition their data, which can increase or decrease the number of partitions. Repartitioning can be used to optimize the performance of an application, especially before performing wide transformations that involve shuffles.\n",
    "- __Partitioner__: For key-value pair RDDs, Spark allows custom partitioning through the use of a `Partitioner` (e.g., `HashPartitioner` or `RangePartitioner`). Custom partitioners can control how data is distributed across partitions based on keys, which is particularly useful for operations that involve grouping or reducing data by key."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Spark programs\n",
    "\n",
    "* \"Defines\" the workflow of the application.\n",
    "* Accesses Spark through the `SparkContext` object\n",
    "    * The `SparkContext` object represents a connection to the  cluster\n",
    "    * Allows creating RDDs\n",
    "* Defines RDDs that are \"allocated\" in the nodes of the cluster\n",
    "* Invokes parallel operations on RDDs\n",
    "\n",
    "<img src=\"images/spark.png\" style=\"background:white; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"50%\">\n",
    "\n",
    "## Python API\n",
    "\n",
    "PySpark uses Py4J that enables Python programs to dynamically access Java objects.\n",
    "\n",
    "<img src=\"images/pyspark.png\" style=\"background:none; border:none; box-shadow:none; display:inline; margin:0; vertical-align:middle;\" width=\"30%\">\n",
    "\n",
    "## RDD Operations\n",
    "There are two kinds of operations on RDDs: __transformations__ and __actions__.\n",
    "\n",
    "Transformations take as input an RDD and produce as output another RDD (you cannot change an existing RDD, they are immutable). Computation of transformations is deferred until an action is executed.\n",
    "\n",
    "An action does not return an RDD, but instead returns data to the driver (for example in the form of a Python list), or writes data to disk or a database.\n",
    "\n",
    "This laziness of executing transformations allows Spark to optimize computations. Only when the user wants real output, the framework will start to compute.\n",
    "\n",
    "Many transformations (and some actions) are based on user provided functions that specify which \"transformation\" function must be applied on the elements of the \"input\" RDD.\n",
    "\n",
    "Each language has its own solution to pass functions to Spark’s transformations and actions. In Python, we can use:\n",
    "- Lambda functions/expressions\n",
    "- Local user defined functions (local `def`s)\n",
    "\n",
    "### RDDs of Key-Value pairs\n",
    "\n",
    "Spark also supports RDDs of key-value pairs. Key-value pairs in Python are represented by means of Python __tuples__ `(x, y)`, where:\n",
    "- `x` is interpreted as the _key_\n",
    "- `y` is interpretedas the _value_\n",
    "\n",
    "RDDs of key-value pairs are characterized by specific operations (`reduceByKey()`, `join()`, etc.). Spark also offers quite a number of `...byKey` and `...byValues` methods that operate on key-value pair RDDs. These operations analyze the content of one group (key) at a time. The basic idea is similar to the one of the MapReduce-based programs in Hadoop.\n",
    "\n",
    "Keys can be of any __hashable__ type, which means all primitive types (numbers, strings, etc.), tuples, but not lists or dictionaries.\n",
    "\n",
    "Values can be of any type.\n",
    "\n",
    "### RDDs of numbers\n",
    "\n",
    "Spark provides specific actions for RDD containing numerical values (integers or floats). The following specific actions are also  available on this type of RDDs:\n",
    "\n",
    "`sum()`, `mean()`, `stdev()`, `variance()`, `max()`, `min()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating SparkContext\n",
    "The `SparkContext` contains all the information about the way Spark is set up. When running on a cluster, the `SparkContext` contains the address of the cluster and will make sure operations on RDDs will be executed there. We can also create a `SparkContext` using __local mode__. This means that Spark will run locally, not on a cluster. It will offer some form of parallelism by making use of the various cores it has available.\n",
    "\n",
    "> Note: For all the available modules the Spark has a separate context object, so Spark 2.0 introduced a `SparkSession`, that provides a single point of entry to interact with Spark functionality and wraps all possible contexts.\n",
    "\n",
    "Spark is best used in cluster mode where it will run on many machines simultaneously. Local mode is only meant for training or testing purposes. However, Spark works quite well in local mode and can be quite powerful. In order to run locally developed code on a cluster, the only thing that needs to be changed is the `SparkContext` and paths to input and output files.\n",
    "\n",
    "Even when working in local mode it is important to think of an RDD as a data structure that is distributed over many machines on a cluster, and is not available locally. The machine that contains the `SparkContext` is called the __driver__. The `SparkContext` will communicate with the cluster manager to make sure that the operations on RDDs will run on the cluster in the form of workers. It is important to realize that the driver is a separate entity from the nodes in the cluster. You can consider this notebook as being the driver.\n",
    "\n",
    "Every `SparkContext` launches a Web UI, by default on port `4040`, that displays useful information about the application. This includes, but not limited to:\n",
    "- A list of scheduler stages and tasks;\n",
    "- A summary of RDD sizes and memory usage;\n",
    "- Environment information;\n",
    "- Information about the running executors.\n",
    "\n",
    "You can access this interface by simply opening `http://[cluster_ip]:4040`, e.g., `http://localhost:4040` for local mode, in a web browser. If multiple `SparkContext`s are running on the same host, they will bind to successive ports beginning with `4040` (`4041`, `4042`, etc).\n",
    "\n",
    "> Note: This information is only available for the duration of the application. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "%pip install pyspark"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark import SparkConf, SparkContext, StorageLevel\n",
    "import sys\n",
    "import os\n",
    "import shutil"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Setting default log level to \"WARN\".\n",
      "To adjust logging level use sc.setLogLevel(newLevel). For SparkR, use setLogLevel(newLevel).\n",
      "24/03/04 14:47:37 WARN NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable\n"
     ]
    }
   ],
   "source": [
    "# Number of cores to use for the application. \n",
    "# This dictates how many CPU cores the application will use on the machine it's running on.\n",
    "number_cores = 2\n",
    "\n",
    "# Amount of memory in gigabytes to allocate to the driver.\n",
    "# The driver memory is the amount of RAM the driver (the process running the user's application) will use.\n",
    "memory_gb = 4\n",
    "\n",
    "# Create a SparkConf object to configure the Spark application.\n",
    "# SparkConf allows you to configure some of the common properties (like application name and master URL) \n",
    "# as well as arbitrary key-value pairs through the set method.\n",
    "conf = (\n",
    "    SparkConf()\n",
    "        # Set the name of the Spark application.\n",
    "        # The name will appear in the Spark web UI and is useful for distinguishing your application \n",
    "        # when multiple applications are running on the same cluster.\n",
    "        .setAppName(\"SparkExample\")\n",
    "        # Set the master URL for the cluster.\n",
    "        # Here, \"local[2]\" means run locally with 2 cores. The format \"local[n]\" tells Spark \n",
    "        # to run locally with n worker threads. In this case, it's set dynamically using \n",
    "        # the number_cores variable. This is typically used for testing or development purposes,\n",
    "        # where you don't have a Spark cluster available.\n",
    "        .setMaster(f\"local[{number_cores}]\")\n",
    "        # Set the amount of memory to allocate to the Spark driver process.\n",
    "        # This is done using the spark.driver.memory property. The \"g\" after the number \n",
    "        # specifies gigabytes. This setting is crucial for performance tuning and preventing \n",
    "        # the driver from running out of memory.\n",
    "        .set(\"spark.driver.memory\", f\"{memory_gb}g\")\n",
    ")\n",
    "\n",
    "# Create a SparkContext object using the configuration specified above.\n",
    "# SparkContext is the entry point to Spark. It represents the connection to a Spark cluster,\n",
    "# and it's used to create RDDs, accumulators, and broadcast variables on that cluster.\n",
    "# Here, we pass the SparkConf object created above to configure the SparkContext.\n",
    "sc = SparkContext(conf=conf)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Working with RDDs\n",
    "There are three ways to create an RDD:\n",
    "- by transforming an existing one;\n",
    "- by reading in data from external storage or files;\n",
    "- by creating an RDD based on a local data structure.\n",
    "\n",
    "The most simple way to create an RDD is to `parallelize` a Python collection. The parallelize method distributes the collection across the cluster."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdd = sc.parallelize([1,2,3,4,5])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ParallelCollectionRDD[0] at readRDDFromFile at PythonRDD.scala:289"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rdd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "doubled = rdd.map(lambda x: x * 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PythonRDD[1] at RDD at PythonRDD.scala:53"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "doubled"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'(2) PythonRDD[1] at RDD at PythonRDD.scala:53 []\\n |  ParallelCollectionRDD[0] at readRDDFromFile at PythonRDD.scala:289 []'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "doubled.toDebugString()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## RDD from text file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "text = sc.textFile(\"./data/sample.txt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Lorem ipsum dolor sit amet, consectetur adipiscing elit.',\n",
       " 'Donec vitae consectetur nisl, vel blandit magna.',\n",
       " 'Pellentesque vel magna faucibus lectus suscipit varius ut id lorem.',\n",
       " 'Nulla pretium diam mauris, vel tincidunt quam tempor non.',\n",
       " 'Praesent non convallis ante. Nunc in velit ut dolor malesuada pellentesque.',\n",
       " 'Nulla ultrices bibendum posuere. Mauris at est pulvinar sapien ultricies commodo.',\n",
       " 'Morbi eu nunc turpis. Nunc interdum sem eget nunc aliquet suscipit.',\n",
       " 'Nullam aliquet varius neque, sit amet condimentum odio euismod at.',\n",
       " 'Donec semper dapibus odio, non luctus sem commodo nec.',\n",
       " 'Quisque at tristique sapien, ut porta nibh.',\n",
       " 'Donec mauris arcu, elementum eu commodo sed, condimentum et enim.',\n",
       " 'In sed nunc neque.',\n",
       " 'Vestibulum a suscipit velit. Sed viverra purus sit amet facilisis vehicula.']"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "text.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Partitions are basic units of parallelism in Spark. By setting `minPartitions`, you're suggesting to Spark how many partitions you'd like the dataset to be split into. This can affect the parallelism of data processing tasks. If your cluster has enough resources, Spark might create more than this minimum if it believes doing so will improve performance.\n",
    "\n",
    "However, setting this number too high can also result in excessive overhead that might degrade performance. The optimal number depends on your specific use case and the configuration of your Spark cluster."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "text = sc.textFile(\"./data/sample.txt\", minPartitions=20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Lorem ipsum dolor sit amet, consectetur adipiscing elit.',\n",
       " 'Donec vitae consectetur nisl, vel blandit magna.',\n",
       " 'Pellentesque vel magna faucibus lectus suscipit varius ut id lorem.',\n",
       " 'Nulla pretium diam mauris, vel tincidunt quam tempor non.',\n",
       " 'Praesent non convallis ante. Nunc in velit ut dolor malesuada pellentesque.',\n",
       " 'Nulla ultrices bibendum posuere. Mauris at est pulvinar sapien ultricies commodo.',\n",
       " 'Morbi eu nunc turpis. Nunc interdum sem eget nunc aliquet suscipit.',\n",
       " 'Nullam aliquet varius neque, sit amet condimentum odio euismod at.',\n",
       " 'Donec semper dapibus odio, non luctus sem commodo nec.',\n",
       " 'Quisque at tristique sapien, ut porta nibh.',\n",
       " 'Donec mauris arcu, elementum eu commodo sed, condimentum et enim.',\n",
       " 'In sed nunc neque.',\n",
       " 'Vestibulum a suscipit velit. Sed viverra purus sit amet facilisis vehicula.']"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "text.collect()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Actions\n",
    "\n",
    "Can return results to the Driver program, i.e., return local (Python) variables.\n",
    "\n",
    "> Note: Pay attention to the size of the returned results because they must be stored in the main memory of the Driver program\n",
    "\n",
    "Or can write the result in the storage (output file/folder). The size of the result can be large in this case since it is directly stored in the (distributed) file system\n",
    "\n",
    "More in later notebook and [documentation](https://spark.apache.org/docs/latest/rdd-programming-guide.html#actions)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = sc.parallelize(range(20))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Collect all elements of the RDD into a list and returns it to the driver program. This action is often used for retrieving small amounts of data for testing or debugging.\n",
    "\n",
    "> Note: Using `collect()` on large datasets as it can exhaust the memory of the driver node."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                                                                                \r"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numbers.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute and return the sum of all elements in the RDD. This is an action that triggers computation over the dataset to aggregate the values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "190"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numbers.sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take the first 3 elements of the RDD and returns them as a list to the driver program. Useful for getting a quick look at a small subset of the data without collecting all of it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 2]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numbers.take(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Return the number of elements in the RDD. This action counts the total number of items in the RDD and is useful for understanding the size of your dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "20"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numbers.count()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Save the contents of the RDD as a text file in the specified output directory. This action exports the data in the RDD to external storage, making it persistent.\n",
    "\n",
    "Each element of the RDD will be saved as a separate line in the text file(s). Spark may create multiple part files in the output directory depending on the number of __partitions__ of the RDD."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                                                                                \r"
     ]
    }
   ],
   "source": [
    "# Define a path for the output directory where the results will be saved.\n",
    "outpath = './output/out_numbers'\n",
    "\n",
    "# Check if the output directory already exists.\n",
    "# os.path.exists checks if the path exists, and os.path.isdir checks if it's a directory.\n",
    "if os.path.exists(outpath) and os.path.isdir(outpath):\n",
    "    # If the directory exists, delete it and its contents to avoid errors when saving new data.\n",
    "    # shutil.rmtree is used for removing directories and their contents.\n",
    "    shutil.rmtree(outpath)\n",
    "\n",
    "numbers.saveAsTextFile(outpath)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transformations\n",
    "\n",
    "* Ooperations on RDDs that return a new RDD (because RDDs are immutable)\n",
    "    * Apply a transformation on the elements of the input RDD(s) and the result of the transformation is “stored in/associated with” a new RDD\n",
    "* Are computed lazily\n",
    "    * transformations are computed only when an action is applied on the RDDs generated by the transformation operations\n",
    "* The graph of dependencies between RDDs represents the information about which  RDDs are used to create a new RDD\n",
    "    * This is called lineage graph\n",
    "    * It is represented as a DAG (Directed Acyclic Graph)\n",
    "\n",
    "Transformations can be categorized into two types based on how they handle data and the implications for distributed computing: narrow transformations and wide transformations. The key difference between these two types of transformations lies in how data is shuffled across the cluster.\n",
    "\n",
    "### Narrow Transformations\n",
    "\n",
    "In narrow transformations, all the data required to compute the records in a single partition of the parent RDD is contained within a single partition of the parent RDD. This means that narrow transformations do not require data to be shuffled across partitions or nodes in the cluster. They are \"narrow\" because they involve a limited scope of data.\n",
    "\n",
    "Examples: `map`, `filter`, `flatMap`, and `union` are common examples of narrow transformations. For instance, applying a `map` function to an RDD to increase each number by 1 does not require information from any other partition.\n",
    "\n",
    "Performance Aspect: Narrow transformations are generally more efficient than wide transformations because they minimize data shuffling and can often be pipelined, meaning Spark can optimize these operations by executing them in memory without writing to disk.\n",
    "\n",
    "### Wide Transformations\n",
    "\n",
    "Wide transformations, on the other hand, require data from multiple partitions to compute the records in a single partition of the result RDD. This usually involves shuffling data across different partitions and possibly across different nodes in the cluster. Wide transformations are \"wide\" because they may involve data from many (or all) partitions of the parent RDD(s).\n",
    "\n",
    "Examples: `groupBy`, `reduceByKey`, `join`, and `distinct` are examples of wide transformations. For example, to perform a `reduceByKey` operation, Spark needs to aggregate all values for each key across the entire dataset, requiring data to be shuffled so that all values for a key are on the same partition.\n",
    "\n",
    "Performance Aspect: Wide transformations are less efficient than narrow transformations because of the data shuffling involved. Shuffling is a costly operation in terms of network I/O and disk usage, and it can significantly impact the performance and scalability of a Spark application. Spark also needs to checkpoint intermediate data to disk during shuffles to ensure fault tolerance, further impacting performance.\n",
    "\n",
    "More in later notebook and [documentation](https://spark.apache.org/docs/latest/rdd-programming-guide.html#transformations)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = sc.parallelize(range(20))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numbers.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the most used transformations is `map`. This is very similar to the __Map__ in MapReduce. The Spark version of Map is a method called `map` defined on an RDD, and takes as input a single function. This function will be applied to each element in the RDD, and Spark will put the result of the application in the output RDD."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "mapped = numbers.map(lambda x: x * 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                                                                                \r"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[0,\n",
       " 1,\n",
       " 2,\n",
       " 3,\n",
       " 4,\n",
       " 5,\n",
       " 6,\n",
       " 7,\n",
       " 8,\n",
       " 9,\n",
       " 10,\n",
       " 11,\n",
       " 12,\n",
       " 13,\n",
       " 14,\n",
       " 15,\n",
       " 16,\n",
       " 17,\n",
       " 18,\n",
       " 19,\n",
       " 0,\n",
       " 1,\n",
       " 2,\n",
       " 3,\n",
       " 4]"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mapped.collect()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Persisting intermediate data\n",
    "The `persist` method in Apache Spark is a key mechanism for optimizing the performance of RDD computations. By default, Spark computes RDDs lazily and re-computes them each time you run an action on them. However, if you have an RDD that will be reused in your application, you can use the persist method to store it in memory (or in memory and disk, depending on the storage level you choose), so Spark can reuse the RDD without having to recompute it from the beginning. This can significantly improve the performance of your Spark application, especially for iterative algorithms or when the computation to generate the RDD is expensive.\n",
    "\n",
    "Since each action triggers all transformations that were performed on the lineage, if you have not designed the jobs to reuse the repeating computations you will see degrade in performance when you are dealing with TBs or PBs of data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "input = sc.parallelize(range(1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = input.map(lambda x: x ** 5)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you persist a dataset, each node stores its partitioned data in memory and reuses them in other actions on that dataset. And PySpark persisted data on nodes are fault-tolerant meaning if any partition of a dataset is lost, it will automatically be recomputed using the original transformations that created it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PythonRDD[20] at RDD at PythonRDD.scala:53"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "input.persist(StorageLevel.MEMORY_ONLY)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### StorageLevel\n",
    "\n",
    "All different storage level PySpark supports are available at `org.apache.spark.storage.StorageLevel` class. The storage level specifies how and where to persist or cache a PySpark RDD.\n",
    "\n",
    "* `MEMORY_ONLY` – This is the default behavior of the RDD `cache()` method and stores the RDD or DataFrame as deserialized objects to JVM memory. When there is not enough memory available it will not save DataFrame of some partitions and these will be re-computed as and when required. This takes more memory, but unlike RDD, this would be slower than MEMORY_AND_DISK level as it recomputes the unsaved partitions.\n",
    "* `MEMORY_ONLY_SER` – This is the same as `MEMORY_ONLY` but the difference being it stores RDD as serialized objects to JVM memory. It takes lesser memory (space-efficient) than `MEMORY_ONLY` as it saves objects as serialized and takes an additional few more CPU cycles in order to deserialize.\n",
    "* `MEMORY_ONLY_2` – Same as `MEMORY_ONLY` storage level but replicate each partition to two cluster nodes.\n",
    "* `MEMORY_ONLY_SER_2` – Same as `MEMORY_ONLY_SER` storage level but replicate each partition to two cluster nodes.\n",
    "* `MEMORY_AND_DISK` – This is the default behavior. In this Storage Level, the RDD will be stored in JVM memory as a deserialized object. When required storage is greater than available memory, it stores some of the excess partitions into a disk and reads the data from the disk when required. It is slower as there is I/O involved.\n",
    "* `MEMORY_AND_DISK_SER` – This is the same as `MEMORY_AND_DISK` storage level difference being it serializes the RDD objects in memory and on disk when space is not available.\n",
    "* `MEMORY_AND_DISK_2` – Same as `MEMORY_AND_DISK` storage level but replicate each partition to two cluster nodes.\n",
    "* `MEMORY_AND_DISK_SER_2` – Same as `MEMORY_AND_DISK_SER` storage level but replicate each partition to two cluster nodes.\n",
    "* `DISK_ONLY` – In this storage level, RDD is stored only on disk and the CPU computation time is high as I/O is involved.\n",
    "* `DISK_ONLY_2` – Same as `DISK_ONLY` storage level but replicate each partition to two cluster nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Remove output dir before doing anything\n",
    "outpath = './output/second_output'\n",
    "if os.path.exists(outpath) and os.path.isdir(outpath):\n",
    "    shutil.rmtree(outpath)\n",
    "\n",
    "result.map(lambda x: \"number: {0}\".format(x)).saveAsTextFile(outpath)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "input.is_cached"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also manually remove an RDD from the cache (if you know you won't use it anymore) by calling the `unpersist` method on it. This can help free up memory for other operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PythonRDD[20] at RDD at PythonRDD.scala:53"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "input.unpersist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "input.is_cached"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stop the Spark Context"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.stop()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "35e50a75acf12e54d10fc4ec181faf7e3047482c971076bcb746266dcf5f0b2d"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
