import time
import random

from kafka import KafkaProducer

def process(batch, producer):
    for line in batch:
        producer.send('iot', bytes(line,'UTF-8'))

if __name__ == "__main__":
    producer = KafkaProducer(
        bootstrap_servers=['localhost:9092'],
    )

    n=20000
    with open("iot_telemetry_data.csv", "r") as f:
        batch=[]
        for line in f:
            batch.append(line)
            if len(batch)==n:
                process(batch, producer)
                batch=[]
                time.sleep(2)
    process(batch, producer)