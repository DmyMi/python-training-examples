# Start kafka

## Download Kafka

`get_kafka.sh` helper script will download the latest Kafka version to your folder.

```bash
cd day_8
bash get_kafka.sh
```

## Start Kafka
`start_kafka.sh` helper script will run the downloaded Kafka in background (daemon) mode.

> Note: it creates a hardcoded `iot` topic

```bash
cd day_8
bash start_kafka.sh
```

## Stop Kafka
`stop_kafka.sh` helper script will remove topic, stop Kafka and remove all the leftover folders (including Ivy libraries cache).

> Note: it tries to remove a hardcoded `iot` topic

```bash
cd day_8
bash stop_kafka.sh
```

# Example data

source: [https://www.kaggle.com/datasets/garystafford/environmental-sensor-data-132k](https://www.kaggle.com/datasets/garystafford/environmental-sensor-data-132k?resource=download)

> Note: Remove header row! and replace all quotes (") with empty strings

## Producer

The `producer.py` file will read from a hardcoded dataset file name (`iot_telemetry_data.csv`) and send messages to hardcoded `iot` Kafka topic.

To run it:
```bash
source ./venv/bin/activate

cd day_8

pip install -r requirements.txt

python producer.py
```