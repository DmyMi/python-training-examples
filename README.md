# Python training

## Setup

To be able to render notebooks in VS Code you need to have `ipykernel` package installed in your virtual environment.
You can run helper script `setup.sh` to generate virtual environment and install the required packages.