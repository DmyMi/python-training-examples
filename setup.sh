#!/bin/bash
python3 -m virtualenv venv
source ./venv/bin/activate
python -m pip install --upgrade pip setuptools wheel
pip install -r requirements.txt