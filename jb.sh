#!/bin/zsh

source ./venv/bin/activate

export PYTHONWARNINGS="ignore"

function book_clean() {
    jupyter-book clean ./
}

function book_build() {
    jupyter-book build ./
}

function book_push() {
    cwd=`pwd`
    cd _build/html
    gsutil -m rsync -r -d ./ gs://$1
    cd $cwd
}